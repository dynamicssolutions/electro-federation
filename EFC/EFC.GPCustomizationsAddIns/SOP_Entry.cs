﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Dexterity.Applications.DynamicsDictionary;
using Microsoft.Dexterity.Bridge;
using Microsoft.Dexterity.Applications;
using EFC.GPCustomizationsAddIns.Entities;

namespace EFC.GPCustomizationsAddIns
{
    public class SOP_Entry
    {
        public static SopEntryForm form = Microsoft.Dexterity.Applications.Dynamics.Forms.SopEntry;
        public static SopEntryForm.SopEntryWindow Window = form.SopEntry;

        public static SopEmailDetailEntryForm frmEmailDet = Microsoft.Dexterity.Applications.Dynamics.Forms.SopEmailDetailEntry;
        public static SopEmailDetailEntryForm.EmailDetailEntryWindow winEmailDet = frmEmailDet.EmailDetailEntry;

        private const string CAPTION = "Microsoft Dynamics GP";
        private static SOP_Entry _instance = null;
        SqlConnection sqlConnection = null;

        public static SOP_Entry Instance
        {
            get
            {
                if (_instance == null) _instance = new SOP_Entry();
                return _instance;
            }
        }

        public void Register()
        {
            Window.CustomerNumber.LeaveAfterOriginal += CustomerNumber_LeaveAfterOriginal;
        }

        private void CustomerNumber_LeaveAfterOriginal(object sender, EventArgs e)
        {
            string emailToAddress = string.Empty;
            bool emailDocumentEnabled = false;
            string customerVendorName = "";
            string customerNumberId = "";
            string documentNumber = "";
            string documentId = "";
            short sopTypeSelected;

            EmailMessage emailMessage = null;
            documentId = Window.DocumentId.Value;
            sopTypeSelected = Window.SopType.Value;
            documentNumber = Window.DocumentNumber.Value;
            customerVendorName = Window.CustomerName.Value;
            customerNumberId = Window.CustomerNumber.Value;

            if (sopTypeSelected == (short)SopType.Invoice)
            {
                try
                {
                    if (!string.IsNullOrEmpty(customerNumberId))
                    {
                        emailDocumentEnabled = GetEmailDocumentEnabled(customerNumberId);
                        emailToAddress = GetCustomerEmailToAddress(customerNumberId);
                        emailMessage = GetEmailMessage(documentId);
                    }

                    if (emailDocumentEnabled && !string.IsNullOrEmpty(emailToAddress) && emailMessage != null)
                    {
                        winEmailDet.Open();
                        winEmailDet.Module.Value = 11;
                        winEmailDet.DocumentType.Value = 3;
                        winEmailDet.MasterId.Value = customerNumberId;
                        winEmailDet.EmailMessageId.Value = documentId;
                        winEmailDet.DocumentNumber.Value = documentNumber;
                        winEmailDet.CustomerVendorName.Value = customerVendorName;
                        winEmailDet.EmailBody.Value = emailMessage.EmailMessageBody;
                        winEmailDet.EmailSubject.Value = emailMessage.EmailMessageSubject;
                        winEmailDet.NextButtonWindowArea.RunValidate();
                        winEmailDet.IsChanged = true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private bool GetEmailDocumentEnabled(string customerNumberId)
        {
            bool emailDocumentEnabled = false;
            try
            {
                sqlConnection = SQLBO.GetConnection(SQLBO.DatabaseEnum.COMPANY);
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@EmailDocumentEnabled", SqlDbType.Bit, 1, ParameterDirection.Output, true, 0, 0, "", DataRowVersion.Current, null);
                sqlParams[1] = new SqlParameter("@EmailCardID", customerNumberId);

                string gpQuery = "SELECT @EmailDocumentEnabled = EmailDocumentEnabled FROM SY04905 WHERE EmailDocumentID = 3 AND MODULE1 = 11 AND EmailCardID =  @EmailCardID";

                Microsoft.ApplicationBlocks.Data.SqlHelper.CommandTimeout = 180;
                int intResp = SqlHelper.ExecuteNonQuery(sqlConnection, System.Data.CommandType.Text, gpQuery, sqlParams);

                if (sqlParams[0].Value != null)
                {
                    emailDocumentEnabled = Convert.ToBoolean(sqlParams[0].Value);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An unexpected error occurred. Please contact the administrator. The error was: " + ex.Message, CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return emailDocumentEnabled;
            }
            finally
            {
                sqlConnection.Close();
            }

            return emailDocumentEnabled;
        }

        private string GetCustomerEmailToAddress(string customerNumberId)
        {
            string emailToAddress = string.Empty;
            try
            {
                sqlConnection = SQLBO.GetConnection(SQLBO.DatabaseEnum.COMPANY);
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@EmailToAddress", System.Data.SqlDbType.VarChar, 15, System.Data.ParameterDirection.Output, true, 0, 0, "", System.Data.DataRowVersion.Current, null);
                sqlParams[1] = new SqlParameter("@MasterID", customerNumberId);

                string gpQuery = "SELECT @EmailToAddress = S.EmailToAddress FROM SY01200 S";
                gpQuery += " INNER JOIN RM00102 A ON S.Master_ID = A.CUSTNMBR AND Master_Type = 'CUS'";
                gpQuery += " INNER JOIN RM00101 C ON C.CUSTNMBR = A.CUSTNMBR AND C.ADRSCODE = A.ADRSCODE";
                gpQuery += " WHERE S.Master_ID = @MasterID";

                Microsoft.ApplicationBlocks.Data.SqlHelper.CommandTimeout = 180;
                int intResp = SqlHelper.ExecuteNonQuery(sqlConnection, System.Data.CommandType.Text, gpQuery, sqlParams);

                if (sqlParams[0].Value != null && !string.IsNullOrEmpty(sqlParams[0].Value.ToString()))
                {
                    emailToAddress = sqlParams[0].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An unexpected error occurred. Please contact the administrator. The error was: " + ex.Message, CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return emailToAddress;
            }
            finally
            {
                sqlConnection.Close();
            }

            return emailToAddress;
        }


        private EmailMessage GetEmailMessage(string emailMessageId)
        {
            string emailToAddress = string.Empty;
            EmailMessage messageId = null;

            try
            {
                sqlConnection = SQLBO.GetConnection(SQLBO.DatabaseEnum.COMPANY);
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@EmailMessageID", emailMessageId);

                string Query = "SELECT	EmailMessageID, EmailMessageBody, EmailMessageSubject FROM SY04901 WHERE EmailMessageID = @EmailMessageID";

                Microsoft.ApplicationBlocks.Data.SqlHelper.CommandTimeout = 180;
                SqlDataReader dbReader = SqlHelper.ExecuteReader(sqlConnection, CommandType.Text, Query, sqlParams);

                if (dbReader.HasRows)
                {
                    while (dbReader.Read())
                    {
                        IDataRecord record = dbReader;
                        messageId = new EmailMessage()
                        {
                            EmailMessageBody = Convert.ToString(record["EmailMessageBody"]),
                            EmailMessageSubject = Convert.ToString(record["EmailMessageSubject"])
                        };

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An unexpected error occurred. Please contact the administrator. The error was: " + ex.Message, CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            finally
            {
                sqlConnection.Close();
            }

            return messageId;
        }

        enum SopType
        {
            Quote = 1,
            Order = 2,
            Invoice = 4,
            Return = 3,
            BackOrder = 5,
            FulfillmentOrder = 6
        }
    }
}
