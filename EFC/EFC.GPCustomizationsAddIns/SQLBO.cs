﻿using System;
using System.Data.SqlClient;
using Microsoft.Dexterity;
using Microsoft.Dexterity.Applications;

namespace EFC.GPCustomizationsAddIns
{
    public class SQLBO
    {
        public enum DatabaseEnum
        {
            DYNAMICS,
            COMPANY
        }

        #region Common
        internal static SqlConnection GetConnection(DatabaseEnum db)
        {
            SqlConnection connection = new SqlConnection();
            int num = GPConnection.Startup();
            GPConnection gpConnection = new GPConnection();
            gpConnection.Init("Arctictech", "&j30A2OS076849GWBO~48.547|647c114^01e>");
            gpConnection.Connect(connection, Dynamics.Globals.SqlDataSourceName.Value, Dynamics.Globals.UserId.Value, Dynamics.Globals.SqlPassword.Value);

            if ((gpConnection.ReturnCode & 1) != 1)
            {
                throw new Exception("Unable to get database connection");
            }
            if (db == DatabaseEnum.DYNAMICS)
            {
                connection.ChangeDatabase(Dynamics.Globals.SystemDatabaseName.Value);
                return connection;
            }


            connection.ChangeDatabase(Dynamics.Globals.IntercompanyId);
            return connection;
        }

        internal static string GetServerName()
        {
            using (SqlConnection cnn = GetConnection(DatabaseEnum.DYNAMICS))
            {
                return cnn.DataSource.Trim();
            }
        }
        #endregion
    }
}