﻿namespace EFC.GPCustomizationsAddIns.Entities
{
    public class EmailMessage
    {
        public string EmailMessageId { get; set; }
        public string EmailMessageSubject { get; set; }
        public string EmailMessageBody { get; set; }
    }
}
