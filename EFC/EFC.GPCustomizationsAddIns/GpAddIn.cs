using Microsoft.Dexterity.Applications;
using Microsoft.Dexterity.Applications.DynamicsDictionary;
using Microsoft.Dexterity.Bridge;
using System;
using System.Windows.Forms;

namespace EFC.GPCustomizationsAddIns
{
    public class GPAddIn : IDexterityAddIn
    {
        // IDexterityAddIn interface
        SopEmailDetailEntryForm sopEmDetails = Dynamics.Forms.SopEmailDetailEntry;

        public void Initialize()
        {
            SOP_Entry.Instance.Register();
        }
    }
}